module mod_io

  implicit none

  private
  public :: read_data

contains

  subroutine read_data(filename, time, adj_close)
    character(*), intent(in) :: filename
    character(:), allocatable, intent(inout) :: time(:)
    real, allocatable, intent(inout) :: adj_close(:)

    integer :: fileunit, n, istat
    real :: mask
    integer :: num_records

    num_records = count_num_records(filename)

    allocate(character(10) :: time(num_records))
    allocate(adj_close(num_records))
    open(newunit=fileunit, file=filename)
    read(fileunit, fmt=*, end=1)
    read_loop: do n = 1, num_records
       read(fileunit, fmt=*, end=1) time(n), mask, mask, mask, mask,&
            & adj_close(n)
    end do read_loop
    print *, 'Closing file'
1   close(fileunit)
  end subroutine read_data

  integer function count_num_records(filename) result(num_records)
    character(*), intent(in) :: filename
    integer :: fileunit
    open(newunit=fileunit, file=filename, action='read')
    ! Read file until EOF is reached in read()
    num_records = 0
    do
       read(fileunit, fmt=*, end=2)
       num_records = num_records + 1
    end do
2   close(fileunit)
  end function count_num_records

end module mod_io
