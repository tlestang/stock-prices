program stock_volatility

  use mod_io, only: read_data
  use mod_calc, only: moving_avg, moving_std

  implicit none

  character(len=4) :: stock_names(10)
  character(100) :: filename
  character(len=:), allocatable :: time(:)
  real, allocatable :: adj_close(:)
  real, allocatable :: adj_close_avg(:)
  real, allocatable :: adj_close_std(:)
  integer, parameter :: win_size = 30

  stock_names = ['AAPL', 'AMZN', 'CRAY', 'CSCO', 'HPQ ', 'IBM ',&
       & 'INTC' , 'MSFT', 'NVDA', 'ORCL']

  filename = '../data/' // stock_names(1) // '.csv'
  call read_data(filename, time,  adj_close)
  adj_close_avg = moving_avg(adj_close, win_size)
  adj_close_std = moving_std(adj_close, win_size)

  write (*, fmt='(a, x, f7.3)') 'Avg is', adj_close_avg(1)
  write (*, fmt='(a, x, f7.3)') 'Avg is', adj_close_std(1)

  deallocate(time, adj_close, adj_close_avg, adj_close_std)

end program stock_volatility
