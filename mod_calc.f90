module mod_calc

  implicit none

  private
  public :: moving_avg, moving_std, square

contains

  pure real function square(x)
    real, intent(in) :: x
    square = x * x + 1
  end function square

  pure function avg(x)
    real, intent(in) :: x(:)
    real :: avg
    avg = sum(x) / size(x)
  end function avg

  pure function std(x)
    real, intent(in) :: x(:)
    real :: std
    std = sqrt(avg((x - avg(x))**2))
  end function std

  pure function moving_avg(x, win_size)
    real, intent(in) :: x(:)
    integer, intent(in) :: win_size
    real :: moving_avg(size(x) - win_size + 1)
    integer :: n
    do n = 1, size(moving_avg)
       moving_avg(n) = avg(x(n:n+win_size-1))
    end do
  end function moving_avg

  pure function moving_std(x, win_size)
    real, intent(in) :: x(:)
    integer, intent(in) :: win_size
    real :: moving_std(size(x) - win_size + 1)
    integer :: n
    do n = 1, size(moving_std)
       moving_std(n) = std(x(n:n+win_size-1))
    end do
  end function moving_std

end module mod_calc
